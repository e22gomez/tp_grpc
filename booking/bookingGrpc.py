import booking_pb2_grpc
import booking_pb2
import showtime_pb2_grpc
import showtime_pb2
from concurrent import futures
import grpc
import json


class BookingServicer(booking_pb2_grpc.BookingServicer):
    def __init__(self):
        with open('{}/data/bookings.json'.format("."), "r") as jsf:
            self.db = json.load(jsf)["bookings"]

    def GetListBookings(self, request, context):
        """
        Get the list of all bookings
        :return: all the bookings (in different responses)
        """
        for booking in self.db:
            yield booking_pb2.BookingData(userid=booking["userid"],dates=booking["dates"])

    def GetBookingsFromUserID(self, request, context):
        """
        Get the bookings from a user id
        :return: all the bookings from the given user
        """
        for booking in self.db:
            if booking["userid"] == request.userid:
                yield booking_pb2.BookingData(userid=booking["userid"], dates=booking["dates"])

    # Nous n'avons pas réussi à implémenter totalement le fonctionnement de cette fonction
    def AddBookingToUser(self, request, context):
        """
        Add booking to a user
        :return: the new booking (or empty if the addition failed)
        """
        error = True
        for booking in self.db:
            if booking["userid"] == request.userid:
                for datesobj in booking["dates"]:
                    if datesobj["date"] == request.date:
                        if request.movie not in datesobj["movies"]:
                            with grpc.insecure_channel('localhost:3002') as channel:
                                stub = showtime_pb2_grpc.ShowtimeStub(channel)
                                showtime = stub.GetShowtimeByDate(showtime_pb2.ShowTimeDate(date=datesobj["date"]))
                                if len(showtime.movieids) != 0 and request.movie in showtime.movieids:
                                    datesobj["movies"].append(request.movie)
                                    error = False
                                channel.close()
                with grpc.insecure_channel('localhost:3002') as channel:
                    stub = showtime_pb2_grpc.ShowtimeStub(channel)
                    showtimes = stub.GetListShowtime(showtime_pb2.EmptyShowtime)
                    find = False
                    for showtime in showtimes:
                        if showtime.date == request.date:
                            booking["userid"].append({"date": request.date, "movies": [request.movie]})
                            find = True
                    if find:
                        error = False
                    channel.close()
        if error:
            return booking_pb2.BookingData(userid="", dates="")
        else:
            return booking_pb2.BookingData(userid=request.userid, dates=[booking_pb2.BookingData.Dates(date=request.date, movies=[request.movie])])


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    booking_pb2_grpc.add_BookingServicer_to_server(BookingServicer(), server)
    server.add_insecure_port('[::]:3006')
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    serve()
