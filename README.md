lien gitlab : https://gitlab.imt-atlantique.fr/e22gomez/tp_grpc.git


# UE-AD-A1-GRPC
**Esteban GOMEZ et Capucine BECHEMIN**

_Ce TP modelise une gestion de séances de cinéma._

TP VERT : terminé

TP BLEU : 75%

TP ROUGE : x

### Mise en place
* Clonez le repository
* Ouvrez ce dossier dans votre IDE
* Pour chacun des services ouvrez un terminal à la racine du TP 
* Entrez les commandes suivantes dans chacun d'eux

`cd .\movie\ ` `py .\movie.py`


`cd .\booking\ ` `py .\bookingGrpc.py`


`cd .\showtime\ ` `py .\showtime.py`


`cd .\user\ ` `py .\user.py`

* Lancer notre classe test client.py dans un autre dernier terminal via `cd .\client\ ` `py .\client.py`


### Documentation des routes des APIs

Nous avons implémenté ces routes dans le TP Rest en voici le lien : 
`https://gitlab.imt-atlantique.fr/c22beche/tp_rest.git`


* Movie
`http://localhost:3200/help`
* Booking
`http://localhost:3201/help`
* Showtimes
`http://localhost:3202/help`
* User
`http://localhost:3203/help`


Pour le TP Bleu nous avons implémenté avec gRPC l'ensemble des fonctions excepté AddBookingToUser() qui est réalisé en partie.
Vous trouverer l'implémentation à booking/bookingGrpc.py. Le test de cette partie est commenté au bas du fichier client.py.
