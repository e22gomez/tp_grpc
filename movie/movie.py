import grpc
from concurrent import futures
import json
from google.protobuf.json_format import MessageToJson
import movie_pb2_grpc
import movie_pb2


# Servicer for movie class
class MovieServicer(movie_pb2_grpc.MovieServicer):
    def __init__(self):
        with open('{}/data/movies.json'.format("."), "r") as jsf:
            self.db = json.load(jsf)["movies"]

    # Get a list of all movies
    def GetListMovies(self, request, context):
        """
         Get the list of all movies
         :return: all the movies (in different responses)
         """
        for movie in self.db:
            yield movie_pb2.MovieData(title=movie['title'], rating=movie['rating'], director=movie['director'],
                                      id=movie['id'])

    # Get a movie by his ID
    def GetMovieByID(self, request, context):
        """
        Get a movie by his id
        :return: the movie with the given id (or empty movie if the movie not exist)
        """
        for movie in self.db:
            if movie['id'] == request.id:
                print("Movie found!")
                return movie_pb2.MovieData(title=movie['title'], rating=movie['rating'], director=movie['director'],
                                           id=movie['id'])
        return movie_pb2.MovieData(title="", rating="", director="", id="")

    def GetMovieByTitle(self, request, context):
        """
        Get a movie by his title
        :return: the movie with the given title (or empty movie if the movie not exist)
        """
        for movie in self.db:
            if movie['title'] == request.title:
                print("Movie found!")
                return movie_pb2.MovieData(title=movie['title'], rating=movie['rating'], director=movie['director'],
                                           id=movie['id'])
        return movie_pb2.MovieData(title="", rating="", director="", id="")

    # Get movies by they rate
    def GetMovieByRate(self, request, context):
        """
         Get a movie by his rate
         :return: all the movies with the given rate (in different responses)
         """
        for movie in self.db:
            if movie['rating'] == round(request.rating, 2):
                yield movie_pb2.MovieData(title=movie['title'], rating=movie['rating'], director=movie['director'],
                                          id=movie['id'])

    def ModifyMovieRate(self, request, context):
        """
        Modify the rate of a movie
        :return: the modified movie (or empty movie if there is an error)
        """
        for movie in self.db:
            if movie['title'] == request.title:
                movie['rating'] = request.rating
                return movie_pb2.MovieData(title=movie['title'], rating=movie['rating'], director=movie['director'],
                                           id=movie['id'])
        return movie_pb2.MovieData(title="", rating=0, director="", id="")

    def AddMovie(self, request, context):
        """
        Add a movie
        :return: the new movie (or empty movie if the movie already exist)
        """
        for movie in self.db:
            if movie["id"] == request.id:
                return movie_pb2.MovieData(title="", rating=0, director="", id="")
        self.db.append(eval(MessageToJson(request)))
        return request

    def RemoveMovie(self, request, context):
        """
        Remove a movie
        :return: the removed movie (or empty movie if the movie not exist)
        """
        for movie in self.db:
            if movie["id"] == request.id:
                self.db.remove(movie)
                return movie_pb2.MovieData(title=movie["title"], rating=movie["rating"], director=movie["director"],
                                           id=movie["id"])
        return movie_pb2.MovieData(title="", rating=0, director="", id="")


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    movie_pb2_grpc.add_MovieServicer_to_server(MovieServicer(), server)
    server.add_insecure_port('[::]:3001')
    server.start()
    server.wait_for_termination()


if __name__ == '__main__':
    serve()
