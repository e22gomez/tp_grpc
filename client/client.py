import grpc
import movie_pb2
import movie_pb2_grpc
import showtime_pb2
import showtime_pb2_grpc
import booking_pb2_grpc
import booking_pb2


#This file is our test file, we tested here all the grpc request


def get_movie_by_id(stub, movieid):
    movie = stub.GetMovieByID(movieid)
    print(movie)


def get_list_movies(stub):
    allmovies = stub.GetListMovies(movie_pb2.Empty())
    for movie in allmovies:
        print("Movie called %s" % movie.title)


def get_movie_by_title(stub, movietitle):
    movie = stub.GetMovieByTitle(movietitle)
    print(movie)


def get_movie_by_rate(stub, movierate):
    movies = stub.GetMovieByRate(movierate)
    for movie in movies:
        print("Movie called %s" % (movie.title))


def modify_movie_rate(stub, movieupdaterate):
    movie = stub.ModifyMovieRate(movieupdaterate)
    print(movie)


def add_movie(stub, movietoadd):
    movie = stub.AddMovie(movietoadd)
    print(movie)


def remove_movie(stub, idmovietoremove):
    movie = stub.RemoveMovie(idmovietoremove)
    print(movie)


def get_list_showtime(stub):
    showtimes = stub.GetListShowtime(showtime_pb2.EmptyShowtime())
    for showtime in showtimes:
        print("Showtime : \nDate : " + str(showtime.date) + "\nMovies id : ")
        for movieid in showtime.movieids:
            print(str(movieid))
        print("--")


def get_showtime_by_date(stub, showtimebydate):
    showtimeMovies = stub.GetShowtimeByDate(showtimebydate)
    print(showtimeMovies.movieids)


def get_list_booking(stub):
    bookings = stub.GetListBookings(booking_pb2.EmptyBooking())
    for booking in bookings:
        print(booking)


def get_booking_by_user_id(stub, bookingbyuserid):
    bookings = stub.GetBookingsFromUserID(bookingbyuserid)
    for booking in bookings:
        print(booking)


def add_booking_to_user(stub, bookingtouser):
    booking = stub.AddBookingToUser(bookingtouser)
    print(booking)


def run():
    with grpc.insecure_channel('localhost:3001') as channel:
        stub = movie_pb2_grpc.MovieStub(channel)

        print("-------------- GetMovieByID --------------")
        movieid = movie_pb2.MovieID(id="a8034f44-aee4-44cf-b32c-74cf452aaaae")
        get_movie_by_id(stub, movieid)
        print("-------------- GetListMovies --------------")
        get_list_movies(stub)
        print("-------------- GetMovieByTitle --------------")
        movietitle = movie_pb2.MovieTitle(title="The Good Dinosaur")
        get_movie_by_title(stub, movietitle)
        print("-------------- GetMovieByRate --------------")
        movierate = movie_pb2.MovieRate(rating=7.4)
        get_movie_by_rate(stub, movierate)
        print("-------------- ModifyMovieRate --------------")
        movieupdaterate = movie_pb2.MovieUpdateRate(title="The Good Dinosaur", rating=3)
        modify_movie_rate(stub, movieupdaterate)
        print("-------------- AddMovie --------------")
        movietoadd = movie_pb2.MovieData(title="newFilm",
                                         rating=10,
                                         director="CapupuAndEsteban",
                                         id="meilleuriddumonde")
        add_movie(stub, movietoadd)
        print("-------------- RemoveMovie --------------")
        movietoremove = movie_pb2.MovieID(id="meilleuriddumonde")
        remove_movie(stub, movietoremove)

    with grpc.insecure_channel('localhost:3002') as channel:
        stub = showtime_pb2_grpc.ShowtimeStub(channel)

        print("-------------- GetListShowTime --------------")
        get_list_showtime(stub)
        print("-------------- GetShowtimeByDate --------------")
        showtimebydate = showtime_pb2.ShowTimeDate(date="20151201")
        get_showtime_by_date(stub, showtimebydate)


    with grpc.insecure_channel('localhost:3006') as channel:
        stub = booking_pb2_grpc.BookingStub(channel)
        print("-------------- GetListBooking --------------")
        get_list_booking(stub)
        print("-------------- GetListBookingByUserID --------------")
        bookingbyuserid = booking_pb2.UserID(userid="garret_heaton")
        get_booking_by_user_id(stub, bookingbyuserid)

        # This is the tests of the blue part, the last function to add booking to user didn't work
        # print("-------------- AddBookingToUser --------------")
        # addbookingtouser = booking_pb2.BookingAdd(userid="garret_heaton", date="20151201", movie="7daf7208-be4d-4944-a3ae-c1c2f516f3e6")
        # add_booking_to_user(stub, addbookingtouser)
    channel.close()

if __name__ == '__main__':
    run()
